#include <SDL.h>
#include <SDL_image.h>

struct Memoria{
	Uint32 posX, posY;
	SDL_Surface *imagen;
	Memoria():posX(0),posY(0){
		char tmp[100];
		imagen = IMG_Load("orphen.jpg");
	}
};

void dibujar1(SDL_Surface *ventana);
void dibujar2(SDL_Surface *ventana, Memoria *memoria);
void dibujar3(SDL_Surface *ventana, Memoria *memoria);
void dibujar(SDL_Surface *ventana, Memoria *memoria){
	dibujar3(ventana,memoria);
}


int main(int argc, char *argv[]){
	SDL_Window *ventana;
	SDL_Renderer *render;
	SDL_Texture *textura;
	SDL_Surface *superficie, *tmp;
	SDL_Event evento;
	Sint32 W,H;
	bool salida = false;

	Memoria *memoria = new Memoria();

	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_PNG);
	ventana = SDL_CreateWindow("Ventana", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,0,0,SDL_WINDOW_FULLSCREEN_DESKTOP);
	SDL_GetWindowSize(ventana,&W,&H);
	render = SDL_CreateRenderer(ventana,-1,0);
	textura = SDL_CreateTexture(render,SDL_PIXELFORMAT_ARGB8888,SDL_TEXTUREACCESS_STREAMING,W,H);
	superficie = SDL_CreateRGBSurface(0,W,H,32,0,0,0,0);
	tmp = memoria->imagen; memoria->imagen = SDL_ConvertSurface(tmp,superficie->format,0); SDL_FreeSurface(tmp);

	while(!salida){
		dibujar(superficie, memoria);
		SDL_UpdateTexture(textura,nullptr,superficie->pixels,superficie->pitch);
		SDL_RenderClear(render);
		SDL_RenderCopy(render,textura,nullptr,nullptr);
		SDL_RenderPresent(render);

		if(SDL_PollEvent(&evento)) switch(evento.type){
		case SDL_QUIT:
			salida = true;
			break;
		case SDL_KEYDOWN:
			switch(evento.key.keysym.sym){
			case SDLK_q:
				salida = true;
				break;
			case SDLK_UP:
				memoria->posY--;
				break;
			case SDLK_DOWN:
				memoria->posY++;
				break;
			case SDLK_RIGHT:
				memoria->posX++;
				break;
			case SDLK_LEFT:
				memoria->posX--;
				break;
			default:;
			}
			break;
		default:;
		}


		SDL_Delay(30);
	}

	IMG_Quit();
	SDL_Quit();
	exit(0);
	return 0;
}

void dibujar3(SDL_Surface *ventana, Memoria *memoria){
	Uint32 i,j,t = SDL_GetTicks()/4;
	SDL_Rect rect {0,0,ventana->w,ventana->h};
	SDL_FillRect(ventana,&rect,SDL_MapRGB(ventana->format,0xff,0xff,0xff));
	for(i = 0; i < ventana->w; i++)
		for(j = 0; j < ventana->h; j++){
			*(Uint32*)((Uint8*)(ventana->pixels) + j*ventana->pitch + i*4) = 
(*(Uint32*)((Uint8*)(memoria->imagen->pixels) + ((j+t)%memoria->imagen->h)*memoria->imagen->pitch + ((i+t)%memoria->imagen->w)*4) & 0xff) +
(*(Uint32*)((Uint8*)(memoria->imagen->pixels) + ((j+t)%memoria->imagen->h)*memoria->imagen->pitch + ((i)%memoria->imagen->w)*4) & 0xff00) +
(*(Uint32*)((Uint8*)(memoria->imagen->pixels) + ((j)%memoria->imagen->h)*memoria->imagen->pitch + ((i+t)%memoria->imagen->w)*4) & 0xff0000);
	}
}

void dibujar2(SDL_Surface *ventana, Memoria *memoria){
	SDL_Rect rect {0,0,ventana->w,ventana->h};
	SDL_FillRect(ventana,&rect,SDL_MapRGB(ventana->format,0xff,0x0,0x0));
	rect.x = rect.w /= 10;
	rect.y = rect.h /= 10;
	rect.x *= memoria->posX;
	rect.y *= memoria->posY;
	SDL_FillRect(ventana,&rect,SDL_MapRGB(ventana->format,0x0,0xff,0x0));
}

void dibujar1(SDL_Surface *ventana){
	SDL_Rect rect {0,0,ventana->w,ventana->h};
	SDL_FillRect(ventana,&rect,SDL_MapRGB(ventana->format,0xff,0x0,0x0));
	rect.x = rect.w /= 10;
	rect.y = rect.h /= 10;
	rect.x *= SDL_GetTicks()/100  % 10;
	rect.y *= SDL_GetTicks()/1000 % 10;
	SDL_FillRect(ventana,&rect,SDL_MapRGB(ventana->format,0x0,0xff,0x0));
}
